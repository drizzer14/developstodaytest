import { setError } from '../index';

export const setErrorAction = payload => {
  return {
    type: setError,
    payload
  };
};
