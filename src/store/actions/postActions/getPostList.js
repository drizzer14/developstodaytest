import { getPostList } from '../index';
import { setErrorAction } from '../errorActions/setErrorAction';
import { getDataRequest } from '../../../utilities';

const getPostListAction = payload => {
  return {
    type: getPostList,
    payload
  };
};

export const getPostListRequest = url => {
  return async dispatch => {
    const res = await getDataRequest(url);

    if (res.length) {
      dispatch(getPostListAction(res));
    } else {
      dispatch(
        setErrorAction({
          type: 'postListFetch',
          message: "Error: can't get to them posts :("
        })
      );
    }
  };
};
