import { getPost } from '../index';
import { setErrorAction } from '../errorActions/setErrorAction';
import { getDataRequest } from '../../../utilities';
import { config } from '../../../config';

const getPostAction = payload => {
  return {
    type: getPost,
    payload
  };
};

export const getPostRequest = id => {
  return async dispatch => {
    const res = await getDataRequest(
      `${config.host}/posts/${id}?_embed=comments`
    );

    if (res.body) {
      dispatch(getPostAction(res));
    } else {
      dispatch(
        setErrorAction({
          type: 'postFetch',
          message: 'Error: unable to find a post :('
        })
      );
    }
  };
};
