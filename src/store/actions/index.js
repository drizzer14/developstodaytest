export const getPostList = 'getPostList';
export const getPost = 'getPost';

export const addComment = 'addComment';

export const setError = 'setError';
