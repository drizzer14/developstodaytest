import { config } from '../../../config';
import { postDataRequest } from '../../../utilities';
import { setErrorAction } from '../errorActions/setErrorAction';
import { addComment } from '../index';

const addCommentAction = payload => {
  return {
    type: addComment,
    payload
  };
};

export const addCommentRequest = payload => {
  return async dispatch => {
    const res = await postDataRequest(`${config.host}/comments`, payload);

    if (res.body) {
      dispatch(addCommentAction(res));
    } else {
      dispatch(
        setErrorAction({
          type: 'commentFetch',
          message: 'Error: failed to add a new comment! :('
        })
      );
    }
  };
};
