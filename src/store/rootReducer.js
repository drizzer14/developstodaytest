import { combineReducers } from 'redux';
import { errorReducer } from './reducers/errorReducer';
import { postsReducer } from './reducers/postsReducer';

export const rootReducer = combineReducers({
  posts: postsReducer,
  error: errorReducer
});
