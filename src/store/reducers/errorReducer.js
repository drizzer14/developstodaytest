import { setError } from '../actions';

export const errorReducer = (state = null, { type, payload }) => {
  switch (type) {
    case setError:
      return payload;
    default:
      return state;
  }
};
