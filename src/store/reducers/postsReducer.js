import { addComment, getPost, getPostList } from '../actions';

export const postsReducer = (
  state = { list: [], selectedPost: null },
  { type, payload }
) => {
  switch (type) {
    case addComment:
      return {
        ...state,
        selectedPost: {
          ...state.selectedPost,
          comments: [...state.selectedPost.comments, payload]
        }
      };
    case getPost:
      return {
        ...state,
        selectedPost: payload
      };
    case getPostList:
      return {
        ...state,
        list: [...payload]
      };
    default:
      return state;
  }
};
