export const postDataRequest = async (url, body) => {
  const res = await fetch(url, {
    method: 'POST',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json'
    }
  });
  const json = await res.json();

  return json;
};
