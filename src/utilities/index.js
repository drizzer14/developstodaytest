import { getDataRequest } from './getDataRequest';
import { postDataRequest } from './postDataRequest';

export { getDataRequest, postDataRequest };
