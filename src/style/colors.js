export const Colors = {
  darkest: '#131313',
  neutral: '#e0e0e0',
  lightest: '#f9f9f9',
  light: '#f5f5f5',
  active: '#5bbfda',
  negative: '#d85b5b'
};
