import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { Typography } from '../../style/index';
import List from '../presentational/styled/list';
import Post from '../presentational/Post';
import NotFound from '../presentational/NotFound';
import { HeadingOne, Small } from '../presentational/styled/typography';
import { isStateReady } from '../../store/selectors/isStateReady';
import { getPostListRequest } from '../../store/actions/postActions/getPostList';
import { config } from '../../config';

const PostListContainer = styled.main`
  margin-bottom: ${Typography.fontSize}px;
`;

class PostList extends Component {
  constructor(props) {
    super(props);

    this.renderPosts = this.renderPosts.bind(this);
  }

  componentDidMount() {
    this.props.getPostList(`${config.host}/posts`);
  }

  renderPosts() {
    return (
      <List alignItems='stretch'>
        {this.props.posts.map(post => {
          return (
            <List.Item key={post.id}>
              <Link to={`/posts/${post.id}`}>
                <Post post={post} item />
              </Link>
            </List.Item>
          );
        })}
      </List>
    );
  }

  render() {
    return this.props.error && this.props.error.type === 'postListFetch' ? (
      <NotFound error={this.props.error.message} />
    ) : (
      <PostListContainer>
        <HeadingOne>Posts</HeadingOne>

        {this.props.isStateReady ? (
          this.renderPosts()
        ) : (
          <Small>Loading...</Small>
        )}
      </PostListContainer>
    );
  }
}

const mapStateToProps = state => {
  return {
    isStateReady: isStateReady(state),
    posts: state.posts.list,
    error: state.error
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getPostList: url => {
      dispatch(getPostListRequest(url));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PostList);
