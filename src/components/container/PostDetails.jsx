import React, { Component } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { config } from '../../config';
import Comment from '../presentational/Comment';
import List from '../presentational/styled/list';
import { getPostRequest } from '../../store/actions/postActions/getPost';
import { getPostListRequest } from '../../store/actions/postActions/getPostList';
import { addCommentRequest } from '../../store/actions/commentActions/addComment';
import { setErrorAction } from '../../store/actions/errorActions/setErrorAction';
import { isPostReady } from '../../store/selectors/isPostReady';
import Post from '../presentational/Post';
import NotFound from '../presentational/NotFound';
import {
  ActiveLink,
  Small,
  HeadingThree,
  HeadingOne,
  BackButton,
  Error
} from '../presentational/styled/typography';
import { Colors, Typography } from '../../style/index';

const CommentsContainer = styled.div`
  margin: ${Typography.fontSize}px 0;
`;

const AddCommentContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: ${Typography.fontSize}px;
`;

const Input = styled.input`
  width: 100%;
  background-color: ${Colors.light};
  border: none;
  padding: ${Typography.fontSize}px;
  border-radius: ${Typography.fontSize / 2}px;
  outline: none;

  ::placeholder {
    color: ${Colors.neutral};
  }
`;

const CommentLink = styled(ActiveLink)`
  padding: 0 ${Typography.fontSize}px;
`;

class PostDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      comment: ''
    };

    this.renderComments = this.renderComments.bind(this);
    this.handleChangeComment = this.handleChangeComment.bind(this);
    this.handleAddComment = this.handleAddComment.bind(this);
  }

  async componentDidMount() {
    await this.props.getPostList(`${config.host}/posts`);
    await this.props.getPost(this.props.match.params.id);
  }

  handleChangeComment(e) {
    this.setState({ comment: e.target.value });
  }

  handleAddComment() {
    if (this.state.comment.trim().length) {
      this.props.addComment({
        postId: this.props.selectedPost.id,
        body: this.state.comment
      });
      this.setState({ comment: '' });
      this.props.setError(null);
    } else {
      this.props.setError({
        type: 'commentEmptyError',
        message: 'Please type something!'
      });
    }
  }

  renderComments() {
    return (
      <CommentsContainer>
        <HeadingThree>
          {this.props.selectedPost.comments.length === 1
            ? `${this.props.selectedPost.comments.length} comment`
            : `${this.props.selectedPost.comments.length} comments`}
        </HeadingThree>
        <List>
          {this.props.selectedPost.comments.map(comment => {
            return (
              <List.Item key={comment.id}>
                <Comment comment={comment} />
              </List.Item>
            );
          })}
        </List>
      </CommentsContainer>
    );
  }

  render() {
    return this.props.isPostReady ? (
      <div>
        <HeadingOne>{`Post #${this.props.selectedPost.id}`}</HeadingOne>

        <Post post={this.props.selectedPost} />
        {this.props.selectedPost.comments.length ? this.renderComments() : ''}

        {this.props.error && this.props.error.type === 'commentEmptyError' && (
          <Error>{this.props.error.message}</Error>
        )}

        <AddCommentContainer>
          <Input
            type='text'
            placeholder='Type your comment, son...'
            onChange={this.handleChangeComment}
            value={this.state.comment}
          />
          <CommentLink onClick={this.handleAddComment}>Comment</CommentLink>
        </AddCommentContainer>

        <Link to='/'>
          <BackButton>Go back</BackButton>
        </Link>
      </div>
    ) : this.props.error && this.props.error.type === 'postFetch' ? (
      <NotFound error={this.props.error.message} />
    ) : (
      <Small>Loading...</Small>
    );
  }
}

const mapStateToProps = state => {
  return {
    isPostReady: isPostReady(state),
    selectedPost: state.posts.selectedPost,
    error: state.error
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getPost: id => {
      dispatch(getPostRequest(id));
    },
    getPostList: url => {
      dispatch(getPostListRequest(url));
    },
    addComment: payload => {
      dispatch(addCommentRequest(payload));
    },
    setError: payload => {
      dispatch(setErrorAction(payload));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PostDetails);
