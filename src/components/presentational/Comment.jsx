import React, { Component } from 'react';
import styled from 'styled-components';
import { Paragraph } from './styled/typography';
import { Colors, Typography } from '../../style';

const CommentContainer = styled.div`
  background-color: ${Colors.light};
  padding: ${Typography.fontSize}px;
  margin-top: ${Typography.fontSize}px;
  border-radius: ${Typography.fontSize / 2}px;
`;

export default class Comment extends Component {
  render() {
    const { body } = this.props.comment;
    return (
      <CommentContainer>
        <Paragraph>{body}</Paragraph>
      </CommentContainer>
    );
  }
}
