import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Error, BackButton } from './styled/typography';

export default class NotFount extends Component {
  render() {
    return (
      <div>
        <Error>{this.props.error || '404 Not Found :('}</Error>

        <Link to='/'>
          <BackButton>Go back</BackButton>
        </Link>
      </div>
    );
  }
}
