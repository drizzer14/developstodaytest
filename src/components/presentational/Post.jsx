import React, { Component } from 'react';
import styled from 'styled-components';
import { Colors, Typography } from '../../style';
import { HeadingThree, Paragraph } from './styled/typography';

const PostContainer = styled.div`
  background-color: ${props => (props.item ? Colors.light : Colors.lightest)};
  padding: ${Typography.fontSize}px;
  margin-top: ${Typography.fontSize}px;
  border-radius: ${Typography.fontSize / 2}px;

  :hover {
    background-color: ${props =>
      props.item ? Colors.neutral : Colors.lightest};
    cursor: ${props => (props.item ? 'pointer' : 'default')};
  }
`;

const PostBody = styled.div`
  margin-top: ${Typography.fontSize / 2}px;
`;

export default class Post extends Component {
  render() {
    const { title, body } = this.props.post;

    return (
      <PostContainer item={this.props.item}>
        <HeadingThree>{title}</HeadingThree>

        <PostBody>
          <Paragraph>{body}</Paragraph>
        </PostBody>
      </PostContainer>
    );
  }
}
