import styled from 'styled-components';

const List = styled.ul`
  display: flex;
  flex-direction: ${props => props.flexDirection || 'column'};
  align-items: ${props => props.alignItems || 'flex-start'};
  justify-content: ${props => props.justifyContent || 'flex-start'};
`;

List.Item = styled.li`
  margin: ${props => props.margin};
  padding: ${props => props.padding};
`;

export default List;
