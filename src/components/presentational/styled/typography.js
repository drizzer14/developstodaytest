import styled from 'styled-components';
import { Colors, Typography } from '../../../style/index';

export const BackButton = styled.p`
  margin-top: ${Typography.fontSize}px;
  color: ${Colors.active};

  :hover {
    color: ${Colors.darkest};
  }
`;

export const Small = styled.small`
  font-size: ${Typography.fontSize * 0.75};
  font-weight: 200;
  color: ${props => props.color || Colors.darkest};
`;

export const Error = styled.small`
  font-size: ${Typography.fontSize * 0.75};
  font-weight: 400;
  color: ${Colors.negative};
`;

export const Paragraph = styled.p`
  font-size: ${Typography.fontSize}px;
  font-weight: 400;
  color: ${props => props.color || Colors.darkest};
`;

export const ActiveLink = styled.a`
  font-size: ${Typography.fontSize}px;
  font-weight: 400;
  color: ${Colors.active};

  :hover {
    color: ${Colors.darkest};
    cursor: pointer;
  }
`;

export const HeadingThree = styled.h3`
  font-size: ${Typography.fontSize}px;
  font-weight: 600;
  color: ${props => props.color || Colors.darkest};
`;

export const HeadingTwo = styled.h2`
  font-size: ${Typography.fontSize * 1.125}px;
  font-weight: 600;
  color: ${props => props.color || Colors.darkest};
`;

export const HeadingOne = styled.h1`
  font-size: ${Typography.fontSize * 1.5}px;
  font-weight: 600;
  color: ${props => props.color || Colors.darkest};
`;
