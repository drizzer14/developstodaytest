import React, { Component } from 'react';
import { createGlobalStyle } from 'styled-components';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Colors, Typography } from '../../style';
import PostList from '../container/PostList';
import PostDetails from '../container/PostDetails';
import NotFound from './NotFound';

const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    text-decoration: none;
    list-style-type: none;
  }
  
  body {
    @import url('https://fonts.googleapis.com/css?family=Montserrat+Alternates:200,400,600&display=swap');
    font-family: ${Typography.fontFamily};
    font-size: ${Typography.fontSize}px;
    line-height: 1.5;
    background-color: ${Colors.lightest};
    padding: ${Typography.fontSize * 2}px 20%;
  }
`;

export default class App extends Component {
  render() {
    return (
      <div className='app'>
        <GlobalStyle />

        <Router>
          <Switch>
            <Route exact path='/' component={PostList} />
            <Route path='/posts/:id' component={PostDetails} />
            <Route component={NotFound} />
          </Switch>
        </Router>
      </div>
    );
  }
}
